import { launcher } from '../../shared/platform/client/launcher.js';

import { AccPlayer } from './AccPlayer.js';

async function launchFunction(container, index) {
    try {
        // init user code
        const client = new AccPlayer({
            container,
        })
        await client.init();
        await client.start();

        // promise contains at least a client
        return Promise.resolve({ client });
    } catch (err) {
        console.error(err);
        return Promise.reject(err);
    }
}

// -------------------------------------------------------------------
// bootstrapping
// -------------------------------------------------------------------
(async function bootstrap() {
    //const container = document.querySelector('#__mobilizing-container');
    const searchParams = new URLSearchParams(window.location.search);
    // enable instanciation of multiple clients in the same page to facilitate
    // development and testing (be careful in production...)
    const clientsCount = parseInt(searchParams.get('emulate'), 10) || 1;

    const launched = await launcher({
        //container,
        launchFunction,
        clientsCount,
    });

    // for debugging purposes:
    // contains {client, player} for a single client
    // ... or an Array<{client, player}>
    window.app = launched;
})();
