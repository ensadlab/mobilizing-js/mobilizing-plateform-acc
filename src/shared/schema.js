const e = {};

export function getDefaultValue(schema, key) {
  const entry = schema[key];
  return entry.default;
}
Object.assign(e, {getDefaultValue});

export function isEvent(schema, key) {
  const entry = schema[key];
  return (typeof entry.event !== 'undefined'
          ? entry.event
          : false);
}
Object.assign(e, {isEvent});

export default e;
