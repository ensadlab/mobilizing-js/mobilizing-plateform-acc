import { AbstractExperience } from '@soundworks/core/server';
import pluginPlatform from '../shared/pluginPlatform.js';

export class ClientExperience extends AbstractExperience {
  constructor({
    server,
    clientTypes,
    features = [],
  } = {}) {
    super(server, clientTypes);

    this.features = features;

    const pluginsRequired = [];
    if(pluginPlatform.isRequiredForFeatures(features) ) {
      pluginsRequired.push('platform');
    }

    // require plugins if needed
    this.plugins = [];
    pluginsRequired.forEach( (pluginName) => {
      this.plugins[pluginName] = this.require(pluginName);
    });
  }

  start() {
    super.start();
  }

  enter(client) {
    super.enter(client);
  }

  exit(client) {
    super.exit(client);
  }
}

export default ClientExperience;
