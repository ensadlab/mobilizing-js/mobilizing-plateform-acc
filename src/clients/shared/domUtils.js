const e = {};

const svgNameSpace = 'http://www.w3.org/2000/svg';

function deleteChildren(node) {
  while(node.lastElementChild) {
    node.removeChild(node.lastElementChild);
  }
}
Object.assign(e, {deleteChildren});

function appendCircle(parent, {
    cx = 0,
    cy = 0,
    r = 50,
    colour = 'red',
    opacity = 1,
} = {}) {
  const circle = document.createElementNS(svgNameSpace, 'circle');
  circle.setAttributeNS(null, 'cx', cx);
  circle.setAttributeNS(null, 'cy', cy);
  circle.setAttributeNS(null, 'r', r);
  circle.setAttributeNS(null, 'style', `fill: ${colour}; opacity: ${opacity}; z-index: -100`);

  parent.appendChild(circle);
}
Object.assign(e, {appendCircle});

function nextFrame() {
  return new Promise(requestAnimationFrame);
}
Object.assign(e, {nextFrame});

export default e;
