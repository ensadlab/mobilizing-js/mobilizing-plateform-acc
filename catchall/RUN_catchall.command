#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

sudo nginx -p "$(pwd)" -c nginx_portal.conf

echo
echo "###################################"
echo "#                                 #"
echo "# Stop with                       #"
echo "#            sudo nginx -s stop   #"
echo "#                                 #"
echo "###################################"
