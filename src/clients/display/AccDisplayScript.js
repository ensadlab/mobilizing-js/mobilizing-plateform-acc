import * as Mobilizing from '@mobilizing/library';

export class AccDisplayScript {

    constructor() {
        this.bricks = new Set();
    }

    preLoad() {
    }

    setup() {
        this.renderer = new Mobilizing.three.RendererThree();

        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.renderer.addCamera(this.camera);
        this.camera.transform.setLocalPositionZ(20);

        const size = this.renderer.getCanvasSize();

        const light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(10, 10, 10);
        this.renderer.addToCurrentScene(light);

        this.meanBrick = new Mobilizing.three.Box({
            "width": 2,
            "height": 2,
            "depth": .5
        });
        this.meanBrick.transform.setLocalPosition(0, -5, 0);
        this.renderer.addToCurrentScene(this.meanBrick);

    }

    update() {
    }

    addBrick() {
        const brickTotal = this.bricks.size;
        console.log();
        const brick = new Mobilizing.three.Box({
            "width": 2,
            "height": 2,
            "depth": .5
        });
        brick.transform.setLocalPosition(brickTotal, 2, 0);
        this.renderer.addToCurrentScene(brick);
        this.bricks.add(brick);
        console.log(brick, this.bricks);
        return brick;
    }

    removeBrick(brick) {
        brick.setVisible(false);
        this.bricks.delete(brick);
        // console.log(this.bricks);
        //TODO effacer le mesh de la mémoire dans la scène
    }

    updateBrick(brick, updates) {
        const { acc } = updates;
        if (acc) {
            brick.transform.setLocalRotationOrder("ZXY");
            brick.transform.setLocalRotation(acc.z, -acc.x, -acc.y + 180);
        }
        //suite du déballage
    }

    updateMeanBrick(meanAcc) {
        console.log(meanAcc);
        this.meanBrick.transform.setLocalRotationOrder("ZXY");
        this.meanBrick.transform.setLocalRotation(meanAcc.z, -meanAcc.x, -meanAcc.y + 180);
    }
}
