## Installation

You should run the script `init_dependencies.bash` in order to download and compile `dnsmasq` binary in `bin/dnsmasq`. This has to be done only once.

## Configuration

You may adapt `dnsmasq_local.conf`, `dnsmasq_public.conf`, `hosts` file to your needs.

Be sure to deactivate other DHCP services, like the one provided by the wifi router. It may give different IP to clients and may also point the clients to other DNS.

To be sure to use you own DNS locally, you can not rely on DHCP. you should change your network configuration. Even better create a new one.

For the *first* network interface, the one used to send queries, change the DNS settings.

- 127.0.0.1 should come first
- You can add public DNS, like French Data Network 80.67.169.12
- Avoid secure DNS, that may block requests, like Cloudfare 1.1.1.1

## Run

Start DNS and DHCP with `DNS_run_public.command`. It will ask a password for sudo operations, and will keep running.

- DHCP service
- DNS service
  - forward queries to upstream DNS
  - listen to static IP and localhost
