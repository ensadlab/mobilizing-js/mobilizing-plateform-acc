import { Client } from '../../shared/platform/client/Client.js';
import * as Mobilizing from "@mobilizing/library";
import { AccDisplayScript } from "./AccDisplayScript.js";

export class AccDisplay extends Client {
    constructor({
        container,
    } = {}) {
        super({
            container,
        });
        this.client = this;
        this.container = container;

        this.context = new Mobilizing.Context();
        this.instance = new AccDisplayScript();
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        this.playerStates = new Map();
        this.bricks = new Map();

        this.context.events.on("setupDone", () => {
            console.log("start");
            this.client.stateManager.observe(async (schemaName, stateId, playerId) => {
                switch (schemaName) {
                    case 'player': {
                        //abonnement au nouveau player qui se connecte
                        const state = await this.client.stateManager.attach(schemaName, stateId);
                        //quand un player part, désabonnement, donc on peut effacer des choses
                        state.onDetach(() => {
                            this.playerStates.delete(playerId);
                            this.removeBrick(playerId);
                        });

                        //lors de l'abonnement, mise à jour des players
                        state.subscribe((updates) => {
                            //mise à jour
                            this.updateBrick(playerId, updates);
                        });

                        //ajout à la liste des états (la Map au dessus)
                        this.playerStates.set(playerId, state);
                        this.addBrick(playerId);
                        break;
                    }
                    default:
                        break;
                }
            });
        });

        //start mobilizing script
        this.context.addComponent(this.instance);
        this.runner = new Mobilizing.Runner({ context: this.context });
    }

    addBrick(playerId) {
        const brick = this.instance.addBrick();
        this.bricks.set(playerId, brick);
        this.updateMeanAcc();
    }

    removeBrick(playerId) {
        const brick = this.bricks.get(playerId);
        this.instance.removeBrick(brick);
        this.updateMeanAcc();
    }

    updateBrick(playerId, updates) {

        const brick = this.bricks.get(playerId);
        this.instance.updateBrick(brick, updates);
        this.updateMeanAcc();
    }

    updateMeanAcc() {
        const meanAcc = { x: 0, y: 0, z: 0 };

        if (this.playerStates.size === 0) {
            this.instance.updateMeanBrick(meanAcc);
            return;
        }

        this.playerStates.forEach((state) => {

            const acc = state.get("acc");
            meanAcc.x += acc.x;
            meanAcc.y += acc.y;
            meanAcc.z += acc.z;
        });

        meanAcc.x /= this.playerStates.size;
        meanAcc.y /= this.playerStates.size;
        meanAcc.z /= this.playerStates.size;

        this.instance.updateMeanBrick(meanAcc);
    }

}
