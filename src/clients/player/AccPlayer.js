import { Client } from '../../shared/platform/client/Client.js';
import * as Mobilizing from "@mobilizing/library";
import { AccPlayerScript } from "./AccPlayerScript.js";

export class AccPlayer extends Client {
    constructor({
        container,
    } = {}) {
        super({
            container,
        });
        this.client = this;
        this.container = container;

        this.context = new Mobilizing.Context();

        //on cable ici avec le player
        const reportCallback = ({ acc }) => {
            //récupère le tableau déclaré dans le schéma
            if (this.playerState) {
                this.playerState.set({ acc });
            }
        };
        this.instance = new AccPlayerScript({ reportCallback });
    }

    async init() {
        await super.init();
        // features may be available

        //check the script for the need of user interaction
        if (this.instance.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }
    }

    async start() {
        await super.start();
        // available features should be ready

        //start mobilizing script
        this.context.addComponent(this.instance);
        this.runner = new Mobilizing.Runner({ context: this.context });

        this.playerState = await this.client.stateManager.create('player', {
            id: this.client.id,
            //va chercher la val par défaut
        });

        this.playerState.subscribe(async (updates) => {
            // process client-side or server-side
            //console.log('updates', updates);
        });

        this.context.events.on("setupDone", () => {
            //Pour le 1er coup, on fait une à mise afin d'avoir le bon état de départ
            console.log("setupDone");
        });
    }
}
