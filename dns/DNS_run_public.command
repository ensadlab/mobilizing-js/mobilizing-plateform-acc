#!/bin/bash

clean_up() {
    echo 'clean up'
    echo "$secret" | sudo -S bash -c "ps auwwx | grep ${configuration_file} | grep -v grep | grep -v sudo | awk '{print \$2}' | xargs kill"
}

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

configuration_file='./dnsmasq_public.conf'

# debug output
dnsmasq_command=(
  "$(pwd)/bin/dnsmasq"
  "--no-daemon"
  "--conf-file=${configuration_file}"
  "--quiet-dhcp"
  "--quiet-dhcp6"
  "--quiet-ra"
  "${@}"
)

# append arguments to command
# --interface=en7 to limit responses to specific network interface

# service
# command="$(pwd)/bin/dnsmasq --keep-in-foreground --conf-file=dnsmasq.conf"


echo
echo "****************************************"
echo "* Please type your password            *"
echo "****************************************"
echo

read -r -s secret

sudo -k
while (( $(echo "$secret" | sudo -S echo 1 || echo 0) == 0 )) ; do
    echo
    echo "****************************************"
    echo "* Please type your password again      *"
    echo "****************************************"
    echo
    read -r -s secret
done

echo "OK"
echo

echo "$secret" | sudo -S rm -f dnsmasq.leases

mkdir -p log

while true ; do
    log_file="log/dnsmasq_run_$(date +"%Y-%m-%d_%H-%M-%S").log"
    date | tee "$log_file"

    echo 'start dnsmasq' | tee -a "$log_file"

    clean_up 2>&1 | tee -a "$log_file"
    echo "$secret" | sudo -S "${dnsmasq_command[@]}" 2>&1 | tee -a "$log_file"
    sleep 1
done
