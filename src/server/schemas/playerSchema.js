export const playerSchema = {
    id: {
        type: 'integer',
        default: null,
        nullable: true,
    },
    //pour l'envoi des infos d'accéléro
    acc:{
        type: 'any',//{x:0, y:0, z:0}
        default: {x:0, y:0, z:0},
    }
};

export default playerSchema;
