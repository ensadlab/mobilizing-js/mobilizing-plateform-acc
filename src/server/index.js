import { Server } from '../shared/platform/server/Server.js';
import globalSchema from './schemas/globalSchema.js';
import playerSchema from './schemas/playerSchema.js';

const server = new Server();

// avoid server crash
process.on('unhandledRejection', (reason, p) => {
  console.log('> Unhandled Promise Rejection');
  console.log(reason);
});

// function to allow for async
(async function launch() {
  try {
    // -------------------------------------------------------------------
    // launch application
    // -------------------------------------------------------------------

    await server.init();
    await server.start();

    server.stateManager.registerSchema('global', globalSchema);
    server.stateManager.registerSchema('player', playerSchema);

    // single instance of global schema, created here
    const globalState = await server.stateManager.create('global');

    const playerStates = new Set();
    server.stateManager.observe(async (schemaName, stateId, nodeId) => {
      switch(schemaName) {
        case 'player': {
          const playerState = await server.stateManager.attach(schemaName, stateId);
          // store the player state into a list
          playerStates.add(playerState);

          playerState.subscribe( (updates) => {
            // process client-side or server-side
            // console.log('player updates', updates);

          }); // udpdates

          // logic to do when the state is deleted
          // (e.g. when the player disconnects)
          playerState.onDetach(() => {
            // clean things
            playerStates.delete(playerState);
          });
          break;
        } // 'player'

        default:
          break;

      }
    });

    globalState.subscribe( (updates) => {

    });

  } catch (err) {
    console.error(err.stack);
  }
})();

