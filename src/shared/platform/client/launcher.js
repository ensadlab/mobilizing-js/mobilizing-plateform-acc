const e = {};

export async function launcher({
    container,
    launchFunction,
    clientsCount = 1,
} = {}) {

    const launchResults = [];

    if (clientsCount <= 1) {
        const launch = await launchFunction(container, 0);
        return launch;
    }

    // special logic for emulated clients (1 click to rule them all)
    for (let i = 0; i < clientsCount; i++) {
        const div = document.createElement('div');
        console.log("div = ", div);
        div.classList.add('emulate');
        container.appendChild(div);

        launchResults.push(launchFunction(div, i));
    }

    const launches = await Promise.all(launchResults);

    const initPlatformBtn = document.createElement('div');
    initPlatformBtn.classList.add('init-platform');
    initPlatformBtn.textContent = 'click to init all';

    function initPlatforms(event) {
        launches.forEach( (launch) => {
            const experience = launch.client.experience;
            if (experience.plugins.platform) {
                experience.plugins.platform.onUserGesture(event)
            }
        });
        initPlatformBtn.removeEventListener('touchend', initPlatforms);
        initPlatformBtn.removeEventListener('mouseup', initPlatforms);
        initPlatformBtn.remove();
    }

    initPlatformBtn.addEventListener('touchend', initPlatforms);
    initPlatformBtn.addEventListener('mouseup', initPlatforms);

    container.appendChild(initPlatformBtn);
    return launches;
}
Object.assign(e, {launcher});

export default e;
