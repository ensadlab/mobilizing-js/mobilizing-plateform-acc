const e = {};

const pluginFeatures = [
    'devicemotion',
];

export function isRequiredForFeatures(features) {
    let isRequired = false;

    for(const feature of features) {
      if(pluginFeatures.includes(feature) ){
        isRequired = true;
        break;
      }
    }

    return isRequired;
}
Object.assign(e, {isRequiredForFeatures});

export function providesFeatures(features) {
    return features.filter( (feature) => pluginFeatures.includes(feature) );
}
Object.assign(e, {providesFeatures});

export default e;
