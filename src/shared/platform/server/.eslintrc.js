const common = require('../../../.eslintrc.js');

module.exports = {
  ...common,
  "env": {
    "node": true,
    "es6": true,
  }
}
